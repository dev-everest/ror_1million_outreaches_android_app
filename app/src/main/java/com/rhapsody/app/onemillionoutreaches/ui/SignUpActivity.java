package com.rhapsody.app.onemillionoutreaches.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rhapsody.app.onemillionoutreaches.R;
import com.rhapsody.app.onemillionoutreaches.asyncTask.UserLoginAsyncTask;
import com.rhapsody.app.onemillionoutreaches.data.ApiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SignUpActivity extends AppCompatActivity {
    TextView tv_signup;
    public static EditText fname, lname, church, email, password;
    Button signup;
    public static Spinner title;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        fname = findViewById(R.id.firstName);
        lname = findViewById(R.id.lastname);
        church = findViewById(R.id.church);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        title = findViewById(R.id.sp_title);
        signup = findViewById(R.id.bt_signup);
        tv_signup = findViewById(R.id.tv_sign_up);
        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSignin();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                String fname = SignUpActivity.fname.getText().toString();
                String lname = SignUpActivity.lname.getText().toString();
                String church = SignUpActivity.church.getText().toString();
                String email = SignUpActivity.email.getText().toString();
                String password = SignUpActivity.password.getText().toString();
                String title = SignUpActivity.title.getSelectedItem().toString();
                String CHURCH_ID = "test";
                startRegister(password, email,fname,lname,church,title, CHURCH_ID);
            }
        });
    }

    public void startSignin(){
        startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
        finish();
    }

    public void startRegister(final String pass, final String email, final String fname, final String lname, final String cname, final String title, final String churchid) {


        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        String url = ApiUtils.API_URL_CREATE_ACCOUNT+"password="+pass+"&email="+email+"&fname="+fname+"&lname="+lname+"&cname="+cname+"&title="+title+"&id="+churchid;
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("JSN1", response);
                try {
                    JSONObject account = new JSONObject(response);
                    String state = account.getString("state");
                    String message = account.getString("message");

                    if(state.contentEquals("true")){
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        SharedPreferences sharedPreferences = getSharedPreferences("UserPref", MODE_PRIVATE);
                        SharedPreferences.Editor myEdit = sharedPreferences.edit();
                        myEdit.putString("fname", fname);
                        myEdit.putString("lname", lname);
                        myEdit.putString("email", email);
                        myEdit.putString("cname", cname);
                        myEdit.putString("title", title);
                        myEdit.putString("churchid", churchid);
                        myEdit.commit();
                        //startActivity(new Intent(SignUpActivity.this, OneMillionActivity.class));
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                    }
                    Log.e("JSN3",state+ message);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Anything you want
                Log.e("JSN2", error.toString());
            }
        });
        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
